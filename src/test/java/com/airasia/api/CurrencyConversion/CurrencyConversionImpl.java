package com.airasia.api.CurrencyConversion;

import org.testng.annotations.Test;

import com.RestAssuredCore.RESTCalls;
import com.bean.request.CurrencyConversionHelper;
import com.bean.request.LoyaltyPointsHelper;
import com.util.TestProperties;
import com.util.URL;

import io.restassured.path.json.JsonPath;
import io.restassured.response.ValidatableResponse;

public class CurrencyConversionImpl {
	
	
@Test
public void getCurrencyConversionAuth() throws Exception {
		
		CurrencyConversionHelper currencyConversionHelper = new CurrencyConversionHelper();
		
		currencyConversionHelper.setClientId(TestProperties.get("clientId"));
		currencyConversionHelper.setClientSecret(TestProperties.get("clientSecret"));
		
		String uri = URL.getEndPoint("/api/v2/currency-conversion-auth");
		ValidatableResponse response = RESTCalls.POSTRequest(uri, currencyConversionHelper);
		System.out.println("******* This is Response ****** " + response);
		JsonPath json = response.statusCode(200).extract().jsonPath();

	}
	

}
