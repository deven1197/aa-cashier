package com.airasia.api.LoyaltyPoint;

import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.RestAssuredCore.RESTCalls;
import com.bean.request.LoyaltyPointsHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.util.TestProperties;
import com.util.URL;

import io.restassured.path.json.JsonPath;
import io.restassured.response.ValidatableResponse;

public class LoyaltyPointsImpl {

	
	public void getBalanceByCurrency() throws Exception {
		
		LoyaltyPointsHelper loyaltyPointsHelper = new LoyaltyPointsHelper();
		
		loyaltyPointsHelper.setMemberId(TestProperties.get("memberId"));
		loyaltyPointsHelper.setCurrencyCode(TestProperties.get("currencyCode"));
		
		String uri = URL.getEndPoint("/get_balance_by_currency");
		System.out.println("---------------- This is URl ----------------  " + uri);
		System.out.println("---------------- This is Body ----------------  " + loyaltyPointsHelper);
		ValidatableResponse response = RESTCalls.POSTRequest(uri, loyaltyPointsHelper);

	}
	
	
	public static void main(String[] args) {
		LoyaltyPointsImpl  loyaltyPointsImpl = new LoyaltyPointsImpl();
		try {
			loyaltyPointsImpl.getBalanceByCurrency();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
