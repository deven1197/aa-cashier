package com.bean.request;

public class CurrencyConversionHelper {

	private String clientId;
	private String clientSecret;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	@Override
	public String toString() {
		return "CurrencyConversionHelper [clientId=" + clientId + ", clientSecret=" + clientSecret + "]";
	}

}
