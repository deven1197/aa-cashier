package com.bean.request;

public class LoyaltyPointsHelper {

	private String memberId;
	private String currencyCode;

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	@Override
	public String toString() {
		return "LoyaltyPointsHelper [memberId=" + memberId + ", currencyCode=" + currencyCode + "]";
	}

}
