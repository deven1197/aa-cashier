package com.RestAssuredCore;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document;

import org.apache.http.HttpStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import com.util.TestProperties;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class RESTCalls {
	/*
	 * This call will be resposible to fire requests
	 */

	private static Logger log = LogManager.getLogger(RESTCalls.class.getName());

	public static Response GETRequest(String uRI) {

		log.info("Inside GETRequest call");
		RequestSpecification requestSpecification = RestAssured.given();
		requestSpecification.contentType(ContentType.JSON);
		Response response = requestSpecification.get(uRI);
		log.debug(requestSpecification.log().all());
		return response;
	}

	public static Response POSTRequest(String uRI, String strJSON) {
		log.info("Inside POSTRequest call");
		RequestSpecification requestSpecification = RestAssured.given().body(strJSON);
		requestSpecification.contentType(ContentType.JSON);
		Response response = requestSpecification.post(uRI);
		log.debug(requestSpecification.log().all());
		return response;
	}

	public static ValidatableResponse POSTRequest(String uri, Object auth) {
		log.info("Inside POSTRequest call");

		ValidatableResponse response = RestAssured.given().accept(ContentType.JSON).with().contentType(ContentType.JSON)
				.body(auth)
				.when().post(uri)
				.then().assertThat().statusCode(HttpStatus.SC_OK).and().
				contentType(ContentType.JSON);
		
		return response;

	}
	

	// POSTRequestBodyValidation - Validates with the response Key & Value

	public static ValidatableResponse POSTRequestBodyValidation(String uri, Object auth, String key, String value) {
		log.info("Inside POSTRequest call");

		ValidatableResponse response = RestAssured.given().accept(ContentType.JSON).with().contentType(ContentType.JSON)
				.body(auth)

				.when().post(uri).then().assertThat().statusCode(HttpStatus.SC_OK).and().body(key, equalTo(value));

		return response;

	}

	public static Response POSTRequest(String uRI, String strJSON, String sessionID) {
		log.info("Inside POSTRequest call");
		RequestSpecification requestSpecification = RestAssured.given().body(strJSON);
		requestSpecification.contentType(ContentType.JSON);
		requestSpecification.header("cookie", "JSESSIONID=" + sessionID + "");
		Response response = requestSpecification.post(uRI);
		log.debug(requestSpecification.log().all());
		return response;
	}

	public static Response PUTRequest(String uRI, String strJSON) {
		log.info("Inside PUTRequest call");
		RequestSpecification requestSpecification = RestAssured.given().body(strJSON);
		requestSpecification.contentType(ContentType.JSON);
		Response response = requestSpecification.put(uRI);
		log.debug(requestSpecification.log().all());
		return response;
	}

	public static Response DELETERequest(String uRI, String strJSON) {
		log.info("Inside DELETERequest call");
		RequestSpecification requestSpecification = RestAssured.given().body(strJSON);
		requestSpecification.contentType(ContentType.JSON);
		Response response = requestSpecification.delete(uRI);
		log.debug(requestSpecification.log().all());
		return response;
	}

	public static ValidatableResponse POSTRequest(String api, String uri, Object auth, RequestSpecification spec,
			RequestFieldsSnippet documentRequestFields, ResponseFieldsSnippet documentResponseFields, String header)
			throws Exception {
		log.info("Inside POSTRequest call");

		ValidatableResponse response = RestAssured.given(spec).log().all().accept(ContentType.JSON).with()
				.contentType(ContentType.JSON).header("accessToken", TestProperties.get(header)).body(auth)
				.filter(document(api, documentRequestFields, documentResponseFields)).when().post(uri).then()
				.assertThat().statusCode(HttpStatus.SC_OK);

		return response;
	}

}
