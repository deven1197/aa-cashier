package com.RestAssuredCore;

import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.documentationConfiguration;

import java.lang.reflect.Method;

//import javax.security.auth.message.callback.PrivateKeyCallback.Request;

import org.springframework.restdocs.ManualRestDocumentation;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class TestNgMethods {
	
	public final ManualRestDocumentation restDocumentation = new ManualRestDocumentation();
	public RequestSpecification spec;

	@BeforeMethod
	public void setUp(Method method) {
		spec = new RequestSpecBuilder().addFilter(documentationConfiguration(restDocumentation)).build();
		restDocumentation.beforeTest(getClass(), method.getName());
	}

	
	@AfterMethod
	public void tearDown() {
		this.restDocumentation.afterTest();
	}
}
