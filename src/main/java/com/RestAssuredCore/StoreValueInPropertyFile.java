package com.RestAssuredCore;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class StoreValueInPropertyFile {
	
	
	public static void storeValue(String inputValue)
	{
	Properties prop = new Properties();
	OutputStream output = null;
	try {
		String currentDirPath = new File("").getAbsolutePath();
		output = new FileOutputStream(currentDirPath+"/conf/dynamicvariables.properties");
		System.out.println("Inside try");
		// set the properties value
		prop.setProperty("authCode", inputValue);
		// save properties to project root folder
		prop.store(output, null);

		
		
	} catch (IOException io) {
		io.printStackTrace();
		System.out.println("Inside catch");
	}
	finally {
		if (output != null) {
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Inside Finally");
	}
	System.out.println("Store value is "+inputValue);


}
	public static void appendValue(String inputValue)
	{
	// set accessToken in property file
    final String FILENAME = new File("").getAbsolutePath()+"/conf/dynamicvariables.properties";
    BufferedWriter bw = null;
	 FileWriter fw = null;
	 try{
	 File file = new File(FILENAME);
	 Properties prop = new Properties();

		// if file does not exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}
       
		// true = append file
		fw = new FileWriter(file.getAbsoluteFile(), true);
		bw = new BufferedWriter(fw);
       bw.newLine();
       bw.append("accessToken=");
       bw.write(inputValue);

		System.out.println("Done");

	} catch (IOException e) {

		e.printStackTrace();

	} finally {

		try {

			if (bw != null)
				bw.close();

			if (fw != null)
				fw.close();

		} catch (IOException ex) {

			ex.printStackTrace();

		}
	}

System.out.println(inputValue);
}
	 


}
