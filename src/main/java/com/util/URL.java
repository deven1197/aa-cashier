package com.util;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import com.util.TestProperties;

public class URL {

	private static Logger log = LogManager.getLogger(URL.class.getName());
	
	
	public static String getEndPoint() throws Exception{
		String URL = TestProperties.get("url");
		log.info("Base URI : " + URL);
		return URL;
	}
	
	public static String getEndPoint(String resource) throws Exception{
		String URL = TestProperties.get("url");
		log.info("URI End Point : " + URL + resource);
		return URL + resource;
	}	
}
