package com.util;
import java.io.File;


public class PathUtil {
	/**
	 * Retrieves the path of the configuration directory
	 * @return path of the configuration directory
	 */
	public static String getConfDirPath() {
		return getProjectHomeDirPath() + "conf";
	}
	/**
	 * Retrieves the path of the home directory of the project
	 * @return path of the home directory of the project
	 */
	public static String getProjectHomeDirPath() {
		String currentDirPath = new File(".").getAbsolutePath();
		return currentDirPath.substring(0, currentDirPath.length() - 1);
	}
}
