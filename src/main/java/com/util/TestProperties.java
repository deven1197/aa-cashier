package com.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import org.apache.commons.io.FileUtils;

public class TestProperties {
	private static Properties properties = null;
	static {
		properties = new Properties();
		
		File confDirectory = new File(PathUtil.getConfDirPath());
		List<File> propertiesFiles = (List<File>) FileUtils.listFiles(confDirectory,
				new String[] { "properties", "jsp" }, true);
		for (File propertiesFile : propertiesFiles) {
			if (propertiesFile.getName().equals("LoyaltyPointsService.properties"))
					 {
				try {
					FileInputStream fis = new FileInputStream(propertiesFile);
					properties.load(fis);
					fis.close();

				} catch (IOException e) {

				}
			}
		}
	}

	public static String get(String key) throws Exception {
		String value = (String) properties.get(key);
		if (value == null) {
			throw new Exception("key " + key + " does not exits in properties files");
		}
		return value;
	}

	public static void set(String key, String value) throws Exception {
		properties.setProperty(key, value);

	}

	public static String removeZero(String random) {
		int i = 0;
		while (i < random.length() && random.charAt(i) == '0') {
			i++;
		}

		StringBuffer stringBuffer = new StringBuffer(random);
		stringBuffer.replace(0, i, "");
		return stringBuffer.toString();
	}

	public static String getRandomNumber() {
		String temp = Long.toString(Math.round(Math.random() * 1000000000));

		String randomNumber = removeZero(temp);

		return randomNumber;

	}

}
